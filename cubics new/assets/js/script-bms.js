$( init );
/*========================================== DRAG AND DROP ==========================================*/
function init() {
    if($(".connected-sortable").length>3){
        return
    }
    $( ".droppable-area1, .droppable-area2,.droppable-area3, .droppable-area4" ).sortable({
        connectWith: ".connected-sortable",
        stack: '.connected-sortable div',

    }).disableSelection();
}

/*========================================== video play==========================================*/
$('.cover').on('click', function () {
    $(".videoTag").css({
        'z-index' : 4,
        'opacity': 1
    });
    $('.videoTag').trigger('play');
});
VideoScrollbarAppear();
    var videoTag = document.querySelector('.videoTag');
    var source = document.getElementById('videoSource');
function myFunction1() {
    videoTag.pause()
    source.setAttribute('src', 'assets/img/cubicsVideo.mp4');
    videoTag.load();
}
function VideoScrollbarAppear() {
    let video = document.querySelector(".videoBg");
    window.addEventListener("scroll", function () {
        let selfContainerTimesLess = document.querySelector(".videoBg")?.offsetHeight / 2.5;
        let selfContainerTimesMore = document.querySelector(".videoBg")?.offsetHeight * 0.2;
        if ((window.scrollY > (document.querySelector(".videoBg")?.offsetTop - selfContainerTimesLess)) && (window.scrollY < (document.querySelector(".customizableInterface")?.offsetTop - selfContainerTimesMore))) {


            setTimeout(function () {
                video?.classList.add("videoScroll")
            }, 500);
        }else{
            setTimeout(function () {
                myFunction1()
                videoTag.style.opacity = "1"
                videoTag.style.zIndex= "-1"
                video?.classList.remove("videoScroll");
            }, 500);
        }
    })
}
/*========================================== Owl carousel Persons ==========================================*/

$('.owl-person').owlCarousel({
    loop:true,
    margin:10,
    responsiveClass:true,
    autoplay:true,
    autoplayTimeout:3000,
    nav: false,
    dots: false,
    center: true,
    mouseDrag: false,
    touchDrag: false,
    responsive:{
        0:{
            items:1,
        },
        600:{
            items:3,
        },
        1000:{
            items:3,
        }
    }
})

/*========================================== Owl carousel Phone inside ==========================================*/
$('.owl-phone').owlCarousel({
    loop:true,
    margin:10,
    responsiveClass:true,
    autoplay:true,
    autoplayTimeout:3000,
    nav: false,
    dots: false,
    mouseDrag: false,
    touchDrag: false,
    responsive:{
        0:{
            items:1,
        },
        600:{
            items:3,
        },
        1000:{
            center: true,
            items:1,
        }
    }
})
// $('.play').on('click',function(){
//     owl.trigger('play.owl.autoplay',[5000])
// })
// $('.stop').on('click',function(){
//     owl.trigger('stop.owl.autoplay')
// })

/*========================================== Business section ==========================================*/
businessManagementScrollbarAppear();

function businessManagementScrollbarAppear() {
    let img = document.querySelector(".secondImg");
    let text = document.querySelector(".secondText");

    window.addEventListener("scroll", function () {
        let selfContainerTimesLess = document.querySelector(".secondSection")?.offsetHeight / 2.5;
        let selfContainerTimesMore = document.querySelector(".secondSection")?.offsetHeight * 0.2;
        if ((window.scrollY > (document.querySelector(".secondSection")?.offsetTop - selfContainerTimesLess)) && (window.scrollY < (document.querySelector(".customizableInterface")?.offsetTop - selfContainerTimesMore))) {
            img.style.bottom = "0";
            img.style.opacity = "1";
            setTimeout(function () {
                text.style.right = "0";
                text.style.opacity = "1";
            }, 500);
        }else{
            img.style.bottom = "-115%";
            img.style.opacity = "1";
            setTimeout(function () {
                text.style.right = "-150%";
                text.style.opacity = "1";
            }, 500);
        }

    })

}

/*========================================== Assignment section ==========================================*/
assignmentScrollAppear();
function assignmentScrollAppear(){
    let persons = document.querySelector('.allPersons');
    window.addEventListener("scroll", function () {
        let selfContainerTimesLess = document.querySelector(".assignmentSection")?.offsetHeight / 2.5;
        let selfContainerTimesMore = document.querySelector(".assignmentSection")?.offsetHeight * 0.2;
        if ((window.scrollY > (document.querySelector(".assignmentSection")?.offsetTop - selfContainerTimesLess)) && (window.scrollY < (document.querySelector(".prjectSection")?.offsetTop - selfContainerTimesMore))) {
            persons?.classList.add('hover');
            setTimeout(function () {
                persons?.classList.add('hover');
            }, 500);
        }else{
            persons?.classList.remove('hover');
            setTimeout(function () {
                persons?.classList.remove('hover');
            }, 500);
        }

    })
}

/*========================================== SmsManagement section ==========================================*/
SmsManagementScrollbarAppear();
function SmsManagementScrollbarAppear() {
    let images = document.querySelector(".smsImgBox");


    window.addEventListener("scroll", function () {
        let selfContainerTimesLess = document.querySelector(".smsSection")?.offsetHeight / 2.5;
        let selfContainerTimesMore = document.querySelector(".smsSection")?.offsetHeight * 0.2;
        if ((window.scrollY > (document.querySelector(".smsSection")?.offsetTop - selfContainerTimesLess)) && (window.scrollY < (document.querySelector(".callCenterSection")?.offsetTop - selfContainerTimesMore))) {
            setTimeout(function () {
                images.classList.add("active")
            }, 500);
        }else{
            // iphone1.style.right = "20%";
            setTimeout(function () {
                images.classList.remove("active")
            }, 500);
        }

    })

}

/*========================================== Analytics section ==========================================*/
AnalyticsScrollbarAppear();
function AnalyticsScrollbarAppear() {
    let abs = document.querySelector(".analyticsImg");


    window.addEventListener("scroll", function () {
        let selfContainerTimesLess = document.querySelector(".analyticsSection")?.offsetHeight / 2.5;
        let selfContainerTimesMore = document.querySelector(".analyticsSection")?.offsetHeight * 0.2;
        if ((window.scrollY > (document.querySelector(".analyticsSection")?.offsetTop - selfContainerTimesLess)) && (window.scrollY < (document.querySelector(".hrSection")?.offsetTop - selfContainerTimesMore))) {
            setTimeout(function () {
                abs?.classList.add("hover");
            }, 500);
        }else{
            // iphone1.style.right = "20%";
            setTimeout(function () {
                abs?.classList.remove("hover");
            }, 500);
        }

    })

}

/*========================================== SalesManagement section ==========================================*/
SalesScrollbarAppear();
function SalesScrollbarAppear() {
    let images = document.querySelector(".relativeImgContainer");


    window.addEventListener("scroll", function () {
        let selfContainerTimesLess = document.querySelector(".salesSection")?.offsetHeight / 2.5;
        let selfContainerTimesMore = document.querySelector(".salesSection")?.offsetHeight * 0.2;
        if ((window.scrollY > (document.querySelector(".salesSection")?.offsetTop - selfContainerTimesLess)) && (window.scrollY < (document.querySelector(".assignmentSection")?.offsetTop - selfContainerTimesMore))) {
            setTimeout(function () {
                images?.classList.add("hover");
            }, 500);
        }else{
            // iphone1.style.right = "20%";
            setTimeout(function () {
                images?.classList.remove("hover");
            }, 500);
        }

    })

}

/*========================================== ProjectManagement section ==========================================*/
ProjectScrollbarAppear();
function ProjectScrollbarAppear() {
    let images = document.querySelector(".phoneText");


    window.addEventListener("scroll", function () {
        let selfContainerTimesLess = document.querySelector(".prjectSection")?.offsetHeight / 2.5;
        let selfContainerTimesMore = document.querySelector(".prjectSection")?.offsetHeight * 0.2;
        if ((window.scrollY > (document.querySelector(".prjectSection")?.offsetTop - selfContainerTimesLess)) && (window.scrollY < (document.querySelector(".financeSection")?.offsetTop - selfContainerTimesMore))) {
            setTimeout(function () {
                images.classList.add("active");
            }, 500);
        }else{
            // iphone1.style.right = "20%";
            setTimeout(function () {
                images.classList.remove("active");
            }, 500);
        }

    })

}

/*========================================== FinanceManagement section ==========================================*/
FinanceScrollbarAppear();
function FinanceScrollbarAppear() {
    let images = document.querySelector(".financeForm");


    window.addEventListener("scroll", function () {
        let selfContainerTimesLess = document.querySelector(".financeSection")?.offsetHeight / 2.5;
        let selfContainerTimesMore = document.querySelector(".financeSection")?.offsetHeight * 0.2;
        if ((window.scrollY > (document.querySelector(".financeSection")?.offsetTop - selfContainerTimesLess)) && (window.scrollY < (document.querySelector(".documentSection")?.offsetTop - selfContainerTimesMore))) {
            setTimeout(function () {
                images.classList.add("active");
            }, 500);
        }else{
            // iphone1.style.right = "20%";
            setTimeout(function () {
                images.classList.remove("active");
            }, 500);
        }

    })

}

/*========================================== Call center section ==========================================*/
callCenterScrollbarAppear();
function callCenterScrollbarAppear() {
    let images = document.querySelector(".calcenterImg");
    window.addEventListener("scroll", function () {
        let selfContainerTimesLess = document.querySelector(".callCenterSection")?.offsetHeight / 2.5;
        let selfContainerTimesMore = document.querySelector(".callCenterSection")?.offsetHeight * 0.2;
        if ((window.scrollY > (document.querySelector(".callCenterSection")?.offsetTop - selfContainerTimesLess)) && (window.scrollY < (document.querySelector(".analyticsSection")?.offsetTop - selfContainerTimesMore))) {
            setTimeout(function () {
                images.classList.add("active");
            }, 500);
        }else{
            setTimeout(function () {
                images.classList.remove("active");
            }, 500);
        }
    })

}

/*========================================== Counter ==========================================*/
$('.counter').each(function() {
    var $this = $(this),
        countTo = $this.attr('data-count');

    $({ countNum: $this.text()}).animate({
            countNum: countTo
        },

        {

            duration: 200000,
            easing:'linear',
            step: function() {
                $this.text(Math.floor(this.countNum));
            },
            complete: function() {
                $this.text(this.countNum);
                //alert('finished');
            }

        });



});

/*========================================== HR section ==========================================*/
hrScrollbarAppear();
function hrScrollbarAppear() {
    let images = document.querySelector(".hrImg");
    // let hrText  = document.querySelectorAll(".textBox");


    window.addEventListener("scroll", function () {
        let selfContainerTimesLess = document.querySelector(".hrSection")?.offsetHeight / 2.5;
        let selfContainerTimesMore = document.querySelector(".hrSection")?.offsetHeight * 0.2;
        if ((window.scrollY > (document.querySelector(".hrSection")?.offsetTop - selfContainerTimesLess)) && (window.scrollY < (document.querySelector(".warehouseSection")?.offsetTop - selfContainerTimesMore))) {
            setTimeout(function () {
                images.classList.add("hover")
            }, 500);
        }else{
            setTimeout(function () {
                images.classList.remove("hover")
            }, 500);
        }
    })

}
/*========================================== warehouse section ==========================================*/
warehouseScrollbarAppear();
function warehouseScrollbarAppear() {
    let image1 = document.querySelector(".warehouseSection");

    window.addEventListener("scroll", function () {
        let selfContainerTimesLess = document.querySelector(".warehouseSection")?.offsetHeight / 2.5;
        let selfContainerTimesMore = document.querySelector(".warehouseSection")?.offsetHeight * 0.2;
        if ((window.scrollY > (document.querySelector(".warehouseSection")?.offsetTop - selfContainerTimesLess)) && (window.scrollY < (document.querySelector(".formSection")?.offsetTop - selfContainerTimesMore))) {
            setTimeout(function () {
                image1?.classList.add("active")
            }, 500);
        }else{
            setTimeout(function () {
                image1?.classList.remove("active")
            }, 500);
        }
    })

}

/*========================================== document section ==========================================*/

$(".docsImg").eq(2).addClass('animation-add-remove-class');
$(".docsImg").eq(2).css("animation-delay", "0s");
setTimeout(function () {
    $(".docsImg").eq(2).removeClass('animation-add-remove-class');
},2000)


$(".docsImg").eq(0).addClass('animation-add-remove-class');
$(".docsImg").eq(0).css("animation-delay", "2s");
setTimeout(function () {
    $(".docsImg").eq(0).removeClass('animation-add-remove-class');
},4000)


$(".docsImg").eq(3).addClass('animation-add-remove-class');
$(".docsImg").eq(3).css("animation-delay", "4s");
setTimeout(function () {
    $(".docsImg").eq(3).removeClass('animation-add-remove-class');
},6000)

$(".docsImg").eq(1).addClass('animation-add-remove-class');
$(".docsImg").eq(1).css("animation-delay", "6s");
setTimeout(function () {
    $(".docsImg").eq(1).removeClass('animation-add-remove-class');
},8000)

$(".docsImg").eq(4).addClass('animation-add-remove-class');
$(".docsImg").eq(4).css("animation-delay", "8s");
setTimeout(function () {
    $(".docsImg").eq(4).removeClass('animation-add-remove-class');
},9050)

setInterval(function () {
    $(".docsImg").removeClass('animation-add-remove-class');

    $(".docsImg").eq(2).addClass('animation-add-remove-class');
    $(".docsImg").eq(2).css("animation-delay", "0s");
    setTimeout(function () {
        $(".docsImg").eq(2).removeClass('animation-add-remove-class');
    },2000)


    $(".docsImg").eq(0).addClass('animation-add-remove-class');
    $(".docsImg").eq(0).css("animation-delay", "2s");
    setTimeout(function () {
        $(".docsImg").eq(0).removeClass('animation-add-remove-class');
    },4000)


    $(".docsImg").eq(3).addClass('animation-add-remove-class');
    $(".docsImg").eq(3).css("animation-delay", "4s");
    setTimeout(function () {
        $(".docsImg").eq(3).removeClass('animation-add-remove-class');
    },6000)

    $(".docsImg").eq(1).addClass('animation-add-remove-class');
    $(".docsImg").eq(1).css("animation-delay", "6s");
    setTimeout(function () {
        $(".docsImg").eq(1).removeClass('animation-add-remove-class');
    },8000)

    $(".docsImg").eq(4).addClass('animation-add-remove-class');
    $(".docsImg").eq(4).css("animation-delay", "8s");
    setTimeout(function () {
        $(".docsImg").eq(4).removeClass('animation-add-remove-class');
    },9050)


}, 10000);

/*========================================== OWL carousel animation section ==========================================*/

// $('.owl-person .owl-item').removeClass('owl-right-element-animation');
// $('.owl-person .owl-item.active.center').next().addClass('owl-right-element-animation');
// $('.owl-person .owl-item').removeClass('owl-left-element-animation');
// $('.owl-person .owl-item.active.center').prev().addClass('owl-left-element-animation');
//
//
//
// setInterval(function () {
//         $('.owl-person .owl-item').removeClass('owl-right-element-animation');
//         $('.owl-person .owl-item.active.center').next().addClass('owl-right-element-animation');
//
//
//         // $('.owl-person .owl-item.active.center').prev().css({
//         //     'transform': 'scale(0.4)',
//         //     'left': '200px'
//         // });
//
//
//         $('.owl-person .owl-item').removeClass('owl-left-element-animation');
//         $('.owl-person .owl-item.active.center').prev().addClass('owl-left-element-animation');
// }, 5000);