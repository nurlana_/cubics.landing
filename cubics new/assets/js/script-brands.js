/*==========================================  ==========================================*/
shopScrollbarAppear();


function shopScrollbarAppear() {
    let img = document.querySelector(".shopImg");
    window.addEventListener("scroll", function () {
        let selfContainerTimesLess = document.querySelector(".shopSection")?.offsetHeight / 2.5;
        let selfContainerTimesMore = document.querySelector(".shopSection")?.offsetHeight * 0.2;
        if ((window.scrollY > (document.querySelector(".shopSection")?.offsetTop - selfContainerTimesLess)) && (window.scrollY < (document.querySelector(".shopSection")?.offsetTop - selfContainerTimesMore))) {
            setTimeout(function () {
                img.classList.add("activeGirl")
            }, 500);
        }else{
            setTimeout(function () {
                img.classList.remove("activeGirl")
            }, 500);
        }

    })
 
}

/*==========================================  ==========================================*/
handsScrollbarAppear();


function handsScrollbarAppear() {
    let img = document.querySelector(".hands");
    window.addEventListener("scroll", function () {
        let selfContainerTimesLess = document.querySelector(".handsSection")?.offsetHeight / 2.5;
        let selfContainerTimesMore = document.querySelector(".handsSection")?.offsetHeight * 0.2;
        if ((window.scrollY > (document.querySelector(".handsSection")?.offsetTop - selfContainerTimesLess)) && (window.scrollY < (document.querySelector(".tabletSection")?.offsetTop - selfContainerTimesMore))) {
            setTimeout(function () {
                img.classList.add("activeHand")
            }, 500);
        }else{
            setTimeout(function () {
                img.classList.remove("activeHand")
            }, 500);
        }

    })

}





/*==========================================  ==========================================*/
tabletScrollbarAppear();


function tabletScrollbarAppear() {
    let img = document.querySelector(".tabletImg");
    window.addEventListener("scroll", function () {
        let selfContainerTimesLess = document.querySelector(".tabletSection")?.offsetHeight / 2.5;
        let selfContainerTimesMore = document.querySelector(".tabletSection")?.offsetHeight * 0.2;
        if ((window.scrollY > (document.querySelector(".tabletSection")?.offsetTop - selfContainerTimesLess)) && (window.scrollY < (document.querySelector(".dynamicMac")?.offsetTop - selfContainerTimesMore))) {
            setTimeout(function () {
                img.classList.add("active")
            }, 500);
        }else{
            setTimeout(function () {
                img.classList.remove("active")
            }, 500);
        }

    })

}