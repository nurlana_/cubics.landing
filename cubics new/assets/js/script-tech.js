/*========================================== EcoSystem section ==========================================*/
ProductScrollbarAppear();


function ProductScrollbarAppear() {
    let hover= document.querySelectorAll(".productImg");

    window.addEventListener("scroll", function () {
        let selfContainerTimesLess = document.querySelector(".cubics-products")?.offsetHeight / 2.5;
        let selfContainerTimesMore = document.querySelector(".cubics-products")?.offsetHeight * 0.2;
        if ((window.scrollY > (document.querySelector(".cubics-products")?.offsetTop - selfContainerTimesLess)) && (window.scrollY < (document.querySelector(".ecoSystem")?.offsetTop - selfContainerTimesMore))) {
            setTimeout(function () {
                hover.forEach(function (element) {
                    element.classList.add("hover")
                })
            }, 500);
        }else{
            setTimeout(function () {
                hover.forEach(function (element) {
                    element.classList.remove("hover")
                })
            }, 500);
        }

    })

}
/*========================================== EcoSystem section ==========================================*/
// EcoSystemScrollbarAppear();
//
//
// function EcoSystemScrollbarAppear() {
//     let hover= document.querySelector(".ecoImg");
//
//     window.addEventListener("scroll", function () {
//         let selfContainerTimesLess = document.querySelector(".ecoSystem")?.offsetHeight / 2.5;
//         let selfContainerTimesMore = document.querySelector(".ecoSystem")?.offsetHeight * 0.2;
//         if ((window.scrollY > (document.querySelector(".ecoSystem")?.offsetTop - selfContainerTimesLess)) && (window.scrollY < (document.querySelector(".advantagesTech")?.offsetTop - selfContainerTimesMore))) {
//             setTimeout(function () {
//                 hover.classList.add("hover")
//             }, 500);
//         }else{
//             setTimeout(function () {
//                 hover.classList.remove("hover")
//             }, 500);
//         }
//
//     })
//
// }

$(".ecoImg").addClass('hover');
setTimeout(function () {
    $(".ecoImg").removeClass('hover');
},3500)
setInterval(function () {
    $(".ecoImg").addClass('hover');
    setTimeout(function () {
        $(".ecoImg").removeClass('hover');
    },3500)

}, 7000);

/*========================================== Advantages section ==========================================*/
// AdvantagesScrollbarAppear();
//
//
// function AdvantagesScrollbarAppear() {
//     let hover= document.querySelector(".rectangle-box");
//     window.addEventListener("scroll", function () {
//         let selfContainerTimesLess = document.querySelector(".advantagesTech")?.offsetHeight / 2.5;
//         let selfContainerTimesMore = document.querySelector(".advantagesTech")?.offsetHeight * 0.2;
//         if ((window.scrollY > (document.querySelector(".advantagesTech")?.offsetTop - selfContainerTimesLess)) && (window.scrollY < (document.querySelector(".brands-carousel")?.offsetTop - selfContainerTimesMore))) {
//             setTimeout(function () {
//                 hover.classList.add("hover1")
//
//             }, 500);
//         }else{
//             setTimeout(function () {
//                 hover.classList.remove("hover1")
//             }, 500);
//         }
//
//     })
//
// }


setTimeout(function () {
    $(".bg-blue").removeClass('blue-pos-4');
    $(".bg-green").removeClass('green-pos-4');
    $(".bg-yellow").removeClass('yellow-pos-4');
    $(".bg-red").removeClass('red-pos-4');
    $(".bg-blue").addClass('blue-pos-1');
    $(".bg-green").addClass('green-pos-1');
    $(".bg-yellow").addClass('yellow-pos-1');
    $(".bg-red").addClass('red-pos-1');
},2000)
setTimeout(function () {
    $(".bg-blue").removeClass('blue-pos-1');
    $(".bg-green").removeClass('green-pos-1');
    $(".bg-yellow").removeClass('yellow-pos-1');
    $(".bg-red").removeClass('red-pos-1');
    $(".bg-blue").addClass('blue-pos-2');
    $(".bg-green").addClass('green-pos-2');
    $(".bg-yellow").addClass('yellow-pos-2');
    $(".bg-red").addClass('red-pos-2');
},4000)
setTimeout(function () {
    $(".bg-blue").removeClass('blue-pos-2');
    $(".bg-green").removeClass('green-pos-2');
    $(".bg-yellow").removeClass('yellow-pos-2');
    $(".bg-red").removeClass('red-pos-2');
    $(".bg-blue").addClass('blue-pos-3');
    $(".bg-green").addClass('green-pos-3');
    $(".bg-yellow").addClass('yellow-pos-3');
    $(".bg-red").addClass('red-pos-3');
},6000)
setTimeout(function () {
    $(".bg-blue").removeClass('blue-pos-3');
    $(".bg-green").removeClass('green-pos-3');
    $(".bg-yellow").removeClass('yellow-pos-3');
    $(".bg-red").removeClass('red-pos-3');
    $(".bg-blue").addClass('blue-pos-4');
    $(".bg-green").addClass('green-pos-4');
    $(".bg-yellow").addClass('yellow-pos-4');
    $(".bg-red").addClass('red-pos-4');
},7950)



setInterval(function () {

    setTimeout(function () {
        $(".bg-blue").removeClass('blue-pos-4');
        $(".bg-green").removeClass('green-pos-4');
        $(".bg-yellow").removeClass('yellow-pos-4');
        $(".bg-red").removeClass('red-pos-4');
        $(".bg-blue").addClass('blue-pos-1');
        $(".bg-green").addClass('green-pos-1');
        $(".bg-yellow").addClass('yellow-pos-1');
        $(".bg-red").addClass('red-pos-1');
    },2000)
    setTimeout(function () {
        $(".bg-blue").removeClass('blue-pos-1');
        $(".bg-green").removeClass('green-pos-1');
        $(".bg-yellow").removeClass('yellow-pos-1');
        $(".bg-red").removeClass('red-pos-1');
        $(".bg-blue").addClass('blue-pos-2');
        $(".bg-green").addClass('green-pos-2');
        $(".bg-yellow").addClass('yellow-pos-2');
        $(".bg-red").addClass('red-pos-2');
    },4000)
    setTimeout(function () {
        $(".bg-blue").removeClass('blue-pos-2');
        $(".bg-green").removeClass('green-pos-2');
        $(".bg-yellow").removeClass('yellow-pos-2');
        $(".bg-red").removeClass('red-pos-2');
        $(".bg-blue").addClass('blue-pos-3');
        $(".bg-green").addClass('green-pos-3');
        $(".bg-yellow").addClass('yellow-pos-3');
        $(".bg-red").addClass('red-pos-3');
    },6000)
    setTimeout(function () {
        $(".bg-blue").removeClass('blue-pos-3');
        $(".bg-green").removeClass('green-pos-3');
        $(".bg-yellow").removeClass('yellow-pos-3');
        $(".bg-red").removeClass('red-pos-3');
        $(".bg-blue").addClass('blue-pos-4');
        $(".bg-green").addClass('green-pos-4');
        $(".bg-yellow").addClass('yellow-pos-4');
        $(".bg-red").addClass('red-pos-4');
    },7950)
}, 8000);
/*========================================== About section ==========================================*/
AboutScrollbarAppear();


function AboutScrollbarAppear() {
    let hover= document.querySelector(".about-img");

    window.addEventListener("scroll", function () {
        let selfContainerTimesLess = document.querySelector(".about-section")?.offsetHeight / 2.5;
        let selfContainerTimesMore = document.querySelector(".about-section")?.offsetHeight * 0.2;
        if ((window.scrollY > (document.querySelector(".about-section")?.offsetTop - selfContainerTimesLess)) && (window.scrollY < (document.querySelector(".news")?.offsetTop - selfContainerTimesMore))) {
            setTimeout(function () {
                hover.classList.add("hover")
            }, 500);
        }else{
            setTimeout(function () {
                hover.classList.remove("hover")
            }, 500);
        }

    })

}
/*========================================== Press section ==========================================*/
$('.owl-brands').owlCarousel({
    loop:true,
    margin:10,
    responsiveClass:true,
    autoplay:true,
    autoplayTimeout:2000,
    nav: false,
    dots: false,
    mouseDrag: false,
    touchDrag: false,
    animateOut: 'fadeOut',
    animateIn: 'fadeIn',
    responsive:{
        0:{
            items:1,
        },
        600:{
            items:3,
        },
        1000:{
            center: true,
            items:1,
        }
    }
})
$('.play').on('click',function(){
    owl.trigger('play.owl.autoplay',[2000])
})
$('.stop').on('click',function(){
    owl.trigger('stop.owl.autoplay')
})